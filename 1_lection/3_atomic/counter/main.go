package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

var counter int64
var wg sync.WaitGroup

func increment() {
	atomic.AddInt64(&counter, 1)
	wg.Done()
}

func main() {
	wg.Add(10)
	for i := 0; i < 10; i++ {
		go increment()
	}
	// Дождемся завершения всех горутин

	wg.Wait()
	fmt.Println("Counter:", atomic.LoadInt64(&counter))
}

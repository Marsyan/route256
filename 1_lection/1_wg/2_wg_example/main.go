package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup

	// Добавление одной задачи в группу
	wg.Add(2)

	// Запуск горутины с выполнением задачи
	go func() {
		defer wg.Done() // Уменьшение счетчика при завершении
		fmt.Println("Выполнение задачи №1")
	}()

	go func() {
		defer wg.Done() // Уменьшение счетчика при завершении
		fmt.Println("Выполнение задачи №2")
	}()

	// Ожидание завершения всех задач в группе
	wg.Wait()

	fmt.Println("Все задачи завершены")
}

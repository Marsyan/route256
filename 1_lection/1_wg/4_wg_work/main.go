package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	var counter int
	wg.Add(1)
	go func() {
		defer wg.Done()
		counter++
	}()
	wg.Wait()
	counter++

	fmt.Printf("counter: %d\n", counter)
}

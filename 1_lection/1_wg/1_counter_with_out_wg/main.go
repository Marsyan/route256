package main

import "fmt"

func main() {
	var counter int
	go func() {
		counter++
	}()
	counter++
	fmt.Printf("counter: %d\n", counter)
}

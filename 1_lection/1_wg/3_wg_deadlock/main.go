package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup

	// Начинаем ожидание группы, но не уменьшаем счетчик
	wg.Add(1)

	// Забыли уменьшить счетчик группы
	// wg.Done()

	// Ожидание завершения группы, что вызовет deadlock
	wg.Wait()

	fmt.Println("Этот код никогда не выполнится из-за deadlock.")
}

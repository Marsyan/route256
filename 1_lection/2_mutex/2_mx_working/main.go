package main

import (
	"fmt"
	"sync"
	"time"
)

type Counter struct {
	counter int
	wg      sync.WaitGroup
	mx      sync.RWMutex
}

func main() {
	c := Counter{}

	c.wg.Add(2)
	go c.increment("goroutine 1")
	go c.increment("goroutine 2")

	c.wg.Wait()
	fmt.Println("Final Counter:", c.counter)
}

func (c *Counter) increment(name string) {
	defer c.wg.Done()
	c.mx.Lock()
	defer c.mx.Unlock()

	for i := 0; i < 2; i++ {
		value := c.counter
		time.Sleep(time.Duration(i) * time.Millisecond)
		value++
		c.counter = value
		fmt.Println(name, "Counter:", c.counter)
	}
}
